com.hybris.table.TableRowsGroup = zk.$extends(zk.Widget, {

	_collapsed: false,

	getZclass : function() {
		return this._zclass != null ? this._zclass : 'y-' + this.widgetName;
	},

	setCollapsed: function (collapsed) {
		this._collapsed = collapsed;
		if (this.desktop) {
			for (var w = this.firstChild; w; w = w.nextSibling) {
				var row = jq(w.$n());
				if (collapsed) {
					row.addClass(w.$s('collapsed'));
				}
				else {
					row.removeClass(w.$s('collapsed'));
				}

				this.fire('onCollapsed', {collapsed: collapsed});
			}
		}
	},

	isCollapsed: function() {
		return this._collapsed;
	},

	redraw: function (out) {
		for (var w = this.firstChild; w; w = w.nextSibling) {
			this._updateChildClasses(w);
			w.redraw(out);
		}
	},

	appendChild: function (w) {
		if (this.desktop) {
			this._updateChildClasses(w);
		}

		this.$supers('appendChild', arguments);
		w.updateRowContents(true);
	},

	setSclass: function (sclass) {
		this.$supers('setSclass', arguments);

		if (this.desktop) {
			this._updateChildClasses(w);
		}
	},

	_updateChildClasses: function(w) {
		if (w == this.firstChild)
		{
			this._appendSclass(w, w.$s('header'));
		}
		if (this._collapsed) {
			this._appendSclass(w, w.$s('collapsed'));
		}
		if (this.getSclass()) {
			this._appendSclass(w, this.getSclass());
		}
	},

	_appendSclass: function(w, sclass) {
		if (this.desktop)
		{
			var row = jq(w.$n());
			row.addClass(sclass);
		}
		else
		{
			var current = w.getSclass();
			if (!current)
			{
				current = '';
			}
			else
			{
				current += ' ';
			}
			w.setSclass(current + sclass);
		}
	},

	getParentTable : function(){
    	return (typeof(this.parent.getParentTable) == "function") ? this.parent.getParentTable() : (this.parent.widgetName == "table" ? this.parent : this.parent.parent);
    },

    updateRowContents : function(){
    	for (var w = this.firstChild; w; w = w.nextSibling) {
    		if(typeof(w.updateRowContents) == "function"){
    			w.updateRowContents();
    		}
    	}
    }
});
