com.hybris.table.TableRow = zk.$extends(zk.Widget, {

	getZclass : function() {
		return this._zclass != null ? this._zclass : 'y-' + this.widgetName;
	},

    redraw : function(out) {
        out.push('<tr ', this.domAttrs_() + '>');
		for (var w = this.firstChild; w; w = w.nextSibling)
			w.redraw(out);
        out.push('</tr>');
        this.updateRowContents(true);
    },

    updateRowContents : function(initial) {
        var _parentTable = this.getParentTable();
        if(initial){
            var _parentTableLeft = typeof(_parentTable.getScroll) == "function" ? _parentTable.getScroll().left : false,
            _row = $("#" + this.uuid),
            _stickyCls = _row.parent().hasClass('y-tableheader') ? '.y-tablecell-header-sticky' : '.y-tablecell-sticky';
            _row.children().removeAttr("style");
            _row.children(_stickyCls).css(_parentTable.getScrollStylingObject(_parentTableLeft, 0));
        }
        _parentTable.requestStickyTableUpdate();
    },

    getParentTable : function(){
    	return (typeof(this.parent.getParentTable) == "function") ? this.parent.getParentTable() : (this.parent.widgetName == "table" ? this.parent : this.parent.parent);
    }
});
