ALTER TABLE PROMOTIONACTION
    ADD COLUMN p_user BIGINT;

ALTER TABLE PROMOTIONACTION
    ADD COLUMN p_usergroup BIGINT BEFORE p_user;

