/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/09/2018 10:07:58                         ---
 * ----------------------------------------------------------------
 */
package yaddonpackage.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedYAddonConstants
{
	public static final String EXTENSIONNAME = "yaddon";
	
	protected GeneratedYAddonConstants()
	{
		// private constructor
	}
	
	
}
