/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.bmecat.parser;

import de.hybris.bootstrap.xml.ObjectProcessor;


/**
 * Provides a call back method used to deliver the results of parsing BMECat tags.
 * 
 * 
 */
public interface BMECatObjectProcessor extends ObjectProcessor
{
	//public void process( TagListener listener, AbstractValueObject obj) throws ParseAbortException;
}
