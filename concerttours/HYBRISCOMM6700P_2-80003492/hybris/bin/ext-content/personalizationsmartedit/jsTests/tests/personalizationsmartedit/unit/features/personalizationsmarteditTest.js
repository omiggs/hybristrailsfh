/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
describe('personalizationsmarteditmodule', function() {
    var mockModules = {};
    setupMockModules(mockModules); // jshint ignore:line

    beforeEach(module('personalizationsmarteditmodule'));
    beforeEach(inject(function() {
        //
    }));

    //    describe('mockModules.featureService.addContextualMenuButton', function() {
    //
    //        it('should have been called', function() {
    //            expect(mockModules.featureService.addContextualMenuButton).toHaveBeenCalled();
    //        });
    //
    //    });
    //
    //    describe('mockModules.decoratorService.addMappings', function() {
    //
    //        it('should have been called', function() {
    //            expect(mockModules.decoratorService.addMappings).toHaveBeenCalled();
    //        });
    //
    //    });
    //
    //    describe('mockModules.featureService.addDecorator', function() {
    //
    //        it('should have been called', function() {
    //            expect(mockModules.featureService.addDecorator).toHaveBeenCalled();
    //        });
    //
    //    });

});
