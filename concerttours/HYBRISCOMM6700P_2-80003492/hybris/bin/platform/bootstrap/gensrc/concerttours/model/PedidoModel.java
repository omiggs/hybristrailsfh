/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/09/2018 10:07:58                         ---
 * ----------------------------------------------------------------
 */
package concerttours.model;

import de.hybris.bootstrap.annotations.Accessor;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import java.util.Date;

/**
 * Generated model class for type Pedido first defined at extension concerttours.
 */
@SuppressWarnings("all")
public class PedidoModel extends ItemModel
{
	/**<i>Generated model type code constant.</i>*/
	public static final String _TYPECODE = "Pedido";
	
	/** <i>Generated constant</i> - Attribute key of <code>Pedido.numero</code> attribute defined at extension <code>concerttours</code>. */
	public static final String NUMERO = "numero";
	
	/** <i>Generated constant</i> - Attribute key of <code>Pedido.nomecliente</code> attribute defined at extension <code>concerttours</code>. */
	public static final String NOMECLIENTE = "nomecliente";
	
	/** <i>Generated constant</i> - Attribute key of <code>Pedido.datapedido</code> attribute defined at extension <code>concerttours</code>. */
	public static final String DATAPEDIDO = "datapedido";
	
	/** <i>Generated constant</i> - Attribute key of <code>Pedido.valorpedido</code> attribute defined at extension <code>concerttours</code>. */
	public static final String VALORPEDIDO = "valorpedido";
	
	/** <i>Generated constant</i> - Attribute key of <code>Pedido.quantidadeitens</code> attribute defined at extension <code>concerttours</code>. */
	public static final String QUANTIDADEITENS = "quantidadeitens";
	
	
	/**
	 * <i>Generated constructor</i> - Default constructor for generic creation.
	 */
	public PedidoModel()
	{
		super();
	}
	
	/**
	 * <i>Generated constructor</i> - Default constructor for creation with existing context
	 * @param ctx the model context to be injected, must not be null
	 */
	public PedidoModel(final ItemModelContext ctx)
	{
		super(ctx);
	}
	
	/**
	 * <i>Generated constructor</i> - for all mandatory and initial attributes.
	 * @deprecated since 4.1.1 Please use the default constructor without parameters
	 * @param _owner initial attribute declared by type <code>Item</code> at extension <code>core</code>
	 */
	@Deprecated
	public PedidoModel(final ItemModel _owner)
	{
		super();
		setOwner(_owner);
	}
	
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.datapedido</code> attribute defined at extension <code>concerttours</code>. 
	 * @return the datapedido
	 */
	@Accessor(qualifier = "datapedido", type = Accessor.Type.GETTER)
	public Date getDatapedido()
	{
		return getPersistenceContext().getPropertyValue(DATAPEDIDO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.nomecliente</code> attribute defined at extension <code>concerttours</code>. 
	 * @return the nomecliente
	 */
	@Accessor(qualifier = "nomecliente", type = Accessor.Type.GETTER)
	public String getNomecliente()
	{
		return getPersistenceContext().getPropertyValue(NOMECLIENTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.numero</code> attribute defined at extension <code>concerttours</code>. 
	 * @return the numero
	 */
	@Accessor(qualifier = "numero", type = Accessor.Type.GETTER)
	public String getNumero()
	{
		return getPersistenceContext().getPropertyValue(NUMERO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.quantidadeitens</code> attribute defined at extension <code>concerttours</code>. 
	 * @return the quantidadeitens
	 */
	@Accessor(qualifier = "quantidadeitens", type = Accessor.Type.GETTER)
	public Integer getQuantidadeitens()
	{
		return getPersistenceContext().getPropertyValue(QUANTIDADEITENS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.valorpedido</code> attribute defined at extension <code>concerttours</code>. 
	 * @return the valorpedido
	 */
	@Accessor(qualifier = "valorpedido", type = Accessor.Type.GETTER)
	public Double getValorpedido()
	{
		return getPersistenceContext().getPropertyValue(VALORPEDIDO);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>Pedido.datapedido</code> attribute defined at extension <code>concerttours</code>. 
	 *  
	 * @param value the datapedido
	 */
	@Accessor(qualifier = "datapedido", type = Accessor.Type.SETTER)
	public void setDatapedido(final Date value)
	{
		getPersistenceContext().setPropertyValue(DATAPEDIDO, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>Pedido.nomecliente</code> attribute defined at extension <code>concerttours</code>. 
	 *  
	 * @param value the nomecliente
	 */
	@Accessor(qualifier = "nomecliente", type = Accessor.Type.SETTER)
	public void setNomecliente(final String value)
	{
		getPersistenceContext().setPropertyValue(NOMECLIENTE, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>Pedido.numero</code> attribute defined at extension <code>concerttours</code>. 
	 *  
	 * @param value the numero
	 */
	@Accessor(qualifier = "numero", type = Accessor.Type.SETTER)
	public void setNumero(final String value)
	{
		getPersistenceContext().setPropertyValue(NUMERO, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>Pedido.quantidadeitens</code> attribute defined at extension <code>concerttours</code>. 
	 *  
	 * @param value the quantidadeitens
	 */
	@Accessor(qualifier = "quantidadeitens", type = Accessor.Type.SETTER)
	public void setQuantidadeitens(final Integer value)
	{
		getPersistenceContext().setPropertyValue(QUANTIDADEITENS, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>Pedido.valorpedido</code> attribute defined at extension <code>concerttours</code>. 
	 *  
	 * @param value the valorpedido
	 */
	@Accessor(qualifier = "valorpedido", type = Accessor.Type.SETTER)
	public void setValorpedido(final Double value)
	{
		getPersistenceContext().setPropertyValue(VALORPEDIDO, value);
	}
	
}
