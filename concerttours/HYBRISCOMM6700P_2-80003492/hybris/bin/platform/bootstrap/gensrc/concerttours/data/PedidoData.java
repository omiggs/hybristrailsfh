/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package concerttours.data;

import java.io.Serializable;

/**
 * Data object do model Pedido
 */
public  class PedidoData  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>PedidoData.numero</code> property defined at extension <code>concerttours</code>. */
		
	private String numero;

	/** <i>Generated property</i> for <code>PedidoData.nome</code> property defined at extension <code>concerttours</code>. */
		
	private String nome;

	/** <i>Generated property</i> for <code>PedidoData.data</code> property defined at extension <code>concerttours</code>. */
		
	private String data;

	/** <i>Generated property</i> for <code>PedidoData.valor</code> property defined at extension <code>concerttours</code>. */
		
	private String valor;

	/** <i>Generated property</i> for <code>PedidoData.quantidade</code> property defined at extension <code>concerttours</code>. */
		
	private String quantidade;
	
	public PedidoData()
	{
		// default constructor
	}
	
		
	
	public void setNumero(final String numero)
	{
		this.numero = numero;
	}

		
	
	public String getNumero() 
	{
		return numero;
	}
	
		
	
	public void setNome(final String nome)
	{
		this.nome = nome;
	}

		
	
	public String getNome() 
	{
		return nome;
	}
	
		
	
	public void setData(final String data)
	{
		this.data = data;
	}

		
	
	public String getData() 
	{
		return data;
	}
	
		
	
	public void setValor(final String valor)
	{
		this.valor = valor;
	}

		
	
	public String getValor() 
	{
		return valor;
	}
	
		
	
	public void setQuantidade(final String quantidade)
	{
		this.quantidade = quantidade;
	}

		
	
	public String getQuantidade() 
	{
		return quantidade;
	}
	


}
