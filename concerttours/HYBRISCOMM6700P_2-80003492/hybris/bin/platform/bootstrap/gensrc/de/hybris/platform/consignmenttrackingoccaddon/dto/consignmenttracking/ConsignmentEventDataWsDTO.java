/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

/**
 * Consignment event
 */
@ApiModel(value="consignmentEvent", description="Consignment event")
public  class ConsignmentEventDataWsDTO  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** date of tracking event<br/><br/><i>Generated property</i> for <code>ConsignmentEventDataWsDTO.eventDate</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="eventDate", value="date of tracking event") 	
	private Date eventDate;

	/** tracking detail<br/><br/><i>Generated property</i> for <code>ConsignmentEventDataWsDTO.detail</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="detail", value="tracking detail") 	
	private String detail;

	/** consignment location<br/><br/><i>Generated property</i> for <code>ConsignmentEventDataWsDTO.location</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="location", value="consignment location") 	
	private String location;

	/** logistics status<br/><br/><i>Generated property</i> for <code>ConsignmentEventDataWsDTO.referenceCode</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="referenceCode", value="logistics status") 	
	private String referenceCode;
	
	public ConsignmentEventDataWsDTO()
	{
		// default constructor
	}
	
		
	
	public void setEventDate(final Date eventDate)
	{
		this.eventDate = eventDate;
	}

		
	
	public Date getEventDate() 
	{
		return eventDate;
	}
	
		
	
	public void setDetail(final String detail)
	{
		this.detail = detail;
	}

		
	
	public String getDetail() 
	{
		return detail;
	}
	
		
	
	public void setLocation(final String location)
	{
		this.location = location;
	}

		
	
	public String getLocation() 
	{
		return location;
	}
	
		
	
	public void setReferenceCode(final String referenceCode)
	{
		this.referenceCode = referenceCode;
	}

		
	
	public String getReferenceCode() 
	{
		return referenceCode;
	}
	


}
