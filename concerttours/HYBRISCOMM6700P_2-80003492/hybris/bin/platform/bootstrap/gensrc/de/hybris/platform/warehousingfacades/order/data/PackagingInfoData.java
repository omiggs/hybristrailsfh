/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.warehousingfacades.order.data;

import java.io.Serializable;

public  class PackagingInfoData  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>PackagingInfoData.width</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String width;

	/** <i>Generated property</i> for <code>PackagingInfoData.height</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String height;

	/** <i>Generated property</i> for <code>PackagingInfoData.length</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String length;

	/** <i>Generated property</i> for <code>PackagingInfoData.grossWeight</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String grossWeight;

	/** <i>Generated property</i> for <code>PackagingInfoData.insuredValue</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String insuredValue;

	/** <i>Generated property</i> for <code>PackagingInfoData.dimensionUnit</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String dimensionUnit;

	/** <i>Generated property</i> for <code>PackagingInfoData.weightUnit</code> property defined at extension <code>warehousingfacades</code>. */
		
	private String weightUnit;
	
	public PackagingInfoData()
	{
		// default constructor
	}
	
		
	
	public void setWidth(final String width)
	{
		this.width = width;
	}

		
	
	public String getWidth() 
	{
		return width;
	}
	
		
	
	public void setHeight(final String height)
	{
		this.height = height;
	}

		
	
	public String getHeight() 
	{
		return height;
	}
	
		
	
	public void setLength(final String length)
	{
		this.length = length;
	}

		
	
	public String getLength() 
	{
		return length;
	}
	
		
	
	public void setGrossWeight(final String grossWeight)
	{
		this.grossWeight = grossWeight;
	}

		
	
	public String getGrossWeight() 
	{
		return grossWeight;
	}
	
		
	
	public void setInsuredValue(final String insuredValue)
	{
		this.insuredValue = insuredValue;
	}

		
	
	public String getInsuredValue() 
	{
		return insuredValue;
	}
	
		
	
	public void setDimensionUnit(final String dimensionUnit)
	{
		this.dimensionUnit = dimensionUnit;
	}

		
	
	public String getDimensionUnit() 
	{
		return dimensionUnit;
	}
	
		
	
	public void setWeightUnit(final String weightUnit)
	{
		this.weightUnit = weightUnit;
	}

		
	
	public String getWeightUnit() 
	{
		return weightUnit;
	}
	


}
