/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.warehousing.data.shipping;

import java.io.Serializable;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

public  class ShippedEntry  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>ShippedEntry.consignmentEntry</code> property defined at extension <code>warehousing</code>. */
		
	private ConsignmentEntryModel consignmentEntry;

	/** <i>Generated property</i> for <code>ShippedEntry.quantity</code> property defined at extension <code>warehousing</code>. */
		
	private Long quantity;
	
	public ShippedEntry()
	{
		// default constructor
	}
	
		
	
	public void setConsignmentEntry(final ConsignmentEntryModel consignmentEntry)
	{
		this.consignmentEntry = consignmentEntry;
	}

		
	
	public ConsignmentEntryModel getConsignmentEntry() 
	{
		return consignmentEntry;
	}
	
		
	
	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

		
	
	public Long getQuantity() 
	{
		return quantity;
	}
	


}
