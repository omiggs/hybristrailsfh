/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:00
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.ruleengineservices.rao;

import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;

public  class ChangeUserGroupRAO extends AbstractRuleActionRAO 
{

 

	/** <i>Generated property</i> for <code>ChangeUserGroupRAO.userGroupId</code> property defined at extension <code>promotionenginetrail</code>. */
		
	private String userGroupId;
	
	public ChangeUserGroupRAO()
	{
		// default constructor
	}
	
		
	
	public void setUserGroupId(final String userGroupId)
	{
		this.userGroupId = userGroupId;
	}

		
	
	public String getUserGroupId() 
	{
		return userGroupId;
	}
	


}
