/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.warehousing.data.sourcing;

public  class FitSourcingLocation extends SourcingLocation 
{

 

	/** <i>Generated property</i> for <code>FitSourcingLocation.fitness</code> property defined at extension <code>warehousing</code>. */
		
	private Double fitness;
	
	public FitSourcingLocation()
	{
		// default constructor
	}
	
		
	
	public void setFitness(final Double fitness)
	{
		this.fitness = fitness;
	}

		
	
	public Double getFitness() 
	{
		return fitness;
	}
	


}
