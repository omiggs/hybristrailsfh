/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.warehouse;

public enum WarehouseConsignmentStatus
{

	/** <i>Generated enum value</i> for <code>WarehouseConsignmentStatus.CANCEL</code> value defined at extension <code>basecommerce</code>. */
	CANCEL , 
	/** <i>Generated enum value</i> for <code>WarehouseConsignmentStatus.PARTIAL</code> value defined at extension <code>basecommerce</code>. */
	PARTIAL , 
	/** <i>Generated enum value</i> for <code>WarehouseConsignmentStatus.COMPLETE</code> value defined at extension <code>basecommerce</code>. */
	COMPLETE  

}
