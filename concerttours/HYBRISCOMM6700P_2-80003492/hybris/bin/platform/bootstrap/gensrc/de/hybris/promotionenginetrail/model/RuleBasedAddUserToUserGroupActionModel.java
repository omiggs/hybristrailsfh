/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/09/2018 10:07:58                         ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.promotionenginetrail.model;

import de.hybris.bootstrap.annotations.Accessor;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.promotionengineservices.model.AbstractRuleBasedPromotionActionModel;
import de.hybris.platform.servicelayer.model.ItemModelContext;

/**
 * Generated model class for type RuleBasedAddUserToUserGroupAction first defined at extension promotionenginetrail.
 */
@SuppressWarnings("all")
public class RuleBasedAddUserToUserGroupActionModel extends AbstractRuleBasedPromotionActionModel
{
	/**<i>Generated model type code constant.</i>*/
	public static final String _TYPECODE = "RuleBasedAddUserToUserGroupAction";
	
	/** <i>Generated constant</i> - Attribute key of <code>RuleBasedAddUserToUserGroupAction.userGroup</code> attribute defined at extension <code>promotionenginetrail</code>. */
	public static final String USERGROUP = "userGroup";
	
	/** <i>Generated constant</i> - Attribute key of <code>RuleBasedAddUserToUserGroupAction.user</code> attribute defined at extension <code>promotionenginetrail</code>. */
	public static final String USER = "user";
	
	
	/**
	 * <i>Generated constructor</i> - Default constructor for generic creation.
	 */
	public RuleBasedAddUserToUserGroupActionModel()
	{
		super();
	}
	
	/**
	 * <i>Generated constructor</i> - Default constructor for creation with existing context
	 * @param ctx the model context to be injected, must not be null
	 */
	public RuleBasedAddUserToUserGroupActionModel(final ItemModelContext ctx)
	{
		super(ctx);
	}
	
	/**
	 * <i>Generated constructor</i> - for all mandatory and initial attributes.
	 * @deprecated since 4.1.1 Please use the default constructor without parameters
	 * @param _owner initial attribute declared by type <code>Item</code> at extension <code>core</code>
	 */
	@Deprecated
	public RuleBasedAddUserToUserGroupActionModel(final ItemModel _owner)
	{
		super();
		setOwner(_owner);
	}
	
	
	/**
	 * <i>Generated method</i> - Getter of the <code>RuleBasedAddUserToUserGroupAction.user</code> attribute defined at extension <code>promotionenginetrail</code>. 
	 * @return the user
	 */
	@Accessor(qualifier = "user", type = Accessor.Type.GETTER)
	public UserModel getUser()
	{
		return getPersistenceContext().getPropertyValue(USER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>RuleBasedAddUserToUserGroupAction.userGroup</code> attribute defined at extension <code>promotionenginetrail</code>. 
	 * @return the userGroup
	 */
	@Accessor(qualifier = "userGroup", type = Accessor.Type.GETTER)
	public UserGroupModel getUserGroup()
	{
		return getPersistenceContext().getPropertyValue(USERGROUP);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>RuleBasedAddUserToUserGroupAction.user</code> attribute defined at extension <code>promotionenginetrail</code>. 
	 *  
	 * @param value the user
	 */
	@Accessor(qualifier = "user", type = Accessor.Type.SETTER)
	public void setUser(final UserModel value)
	{
		getPersistenceContext().setPropertyValue(USER, value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of <code>RuleBasedAddUserToUserGroupAction.userGroup</code> attribute defined at extension <code>promotionenginetrail</code>. 
	 *  
	 * @param value the userGroup
	 */
	@Accessor(qualifier = "userGroup", type = Accessor.Type.SETTER)
	public void setUserGroup(final UserGroupModel value)
	{
		getPersistenceContext().setPropertyValue(USERGROUP, value);
	}
	
}
