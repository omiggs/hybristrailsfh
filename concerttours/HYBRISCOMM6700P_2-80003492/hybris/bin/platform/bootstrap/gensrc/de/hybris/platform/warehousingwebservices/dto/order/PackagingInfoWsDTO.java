/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:02
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.warehousingwebservices.dto.order;

import java.io.Serializable;

public  class PackagingInfoWsDTO  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.width</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String width;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.height</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String height;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.length</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String length;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.grossWeight</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String grossWeight;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.insuredValue</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String insuredValue;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.dimensionUnit</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String dimensionUnit;

	/** <i>Generated property</i> for <code>PackagingInfoWsDTO.weightUnit</code> property defined at extension <code>warehousingwebservices</code>. */
		
	private String weightUnit;
	
	public PackagingInfoWsDTO()
	{
		// default constructor
	}
	
		
	
	public void setWidth(final String width)
	{
		this.width = width;
	}

		
	
	public String getWidth() 
	{
		return width;
	}
	
		
	
	public void setHeight(final String height)
	{
		this.height = height;
	}

		
	
	public String getHeight() 
	{
		return height;
	}
	
		
	
	public void setLength(final String length)
	{
		this.length = length;
	}

		
	
	public String getLength() 
	{
		return length;
	}
	
		
	
	public void setGrossWeight(final String grossWeight)
	{
		this.grossWeight = grossWeight;
	}

		
	
	public String getGrossWeight() 
	{
		return grossWeight;
	}
	
		
	
	public void setInsuredValue(final String insuredValue)
	{
		this.insuredValue = insuredValue;
	}

		
	
	public String getInsuredValue() 
	{
		return insuredValue;
	}
	
		
	
	public void setDimensionUnit(final String dimensionUnit)
	{
		this.dimensionUnit = dimensionUnit;
	}

		
	
	public String getDimensionUnit() 
	{
		return dimensionUnit;
	}
	
		
	
	public void setWeightUnit(final String weightUnit)
	{
		this.weightUnit = weightUnit;
	}

		
	
	public String getWeightUnit() 
	{
		return weightUnit;
	}
	


}
