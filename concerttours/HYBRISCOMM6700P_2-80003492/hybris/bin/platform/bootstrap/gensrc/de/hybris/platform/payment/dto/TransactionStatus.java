/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:02
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.payment.dto;

public enum TransactionStatus
{

	/** <i>Generated enum value</i> for <code>TransactionStatus.ACCEPTED</code> value defined at extension <code>payment</code>. */
	ACCEPTED , 
	/** <i>Generated enum value</i> for <code>TransactionStatus.ERROR</code> value defined at extension <code>payment</code>. */
	ERROR , 
	/** <i>Generated enum value</i> for <code>TransactionStatus.REJECTED</code> value defined at extension <code>payment</code>. */
	REJECTED , 
	/** <i>Generated enum value</i> for <code>TransactionStatus.REVIEW</code> value defined at extension <code>payment</code>. */
	REVIEW  

}
