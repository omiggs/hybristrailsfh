/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:02
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking;

import java.io.Serializable;
import de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking.CarrierWsDTO;
import de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking.ConsignmentEventDataWsDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;

/**
 * Consignment tracking data
 */
@ApiModel(value="consignmentData", description="Consignment tracking data")
public  class ConsignmentDataWsDTO  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** consignment status<br/><br/><i>Generated property</i> for <code>ConsignmentDataWsDTO.statusDisplay</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="statusDisplay", value="consignment status", example="SHIPPED") 	
	private String statusDisplay;

	/** carrier details<br/><br/><i>Generated property</i> for <code>ConsignmentDataWsDTO.carrierDetails</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="carrierDetails", value="carrier details") 	
	private CarrierWsDTO carrierDetails;

	/** tracking id<br/><br/><i>Generated property</i> for <code>ConsignmentDataWsDTO.trackingID</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="trackingID", value="tracking id") 	
	private String trackingID;

	/** the tracking url provided by the carrier<br/><br/><i>Generated property</i> for <code>ConsignmentDataWsDTO.trackingUrl</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="trackingUrl", value="the tracking url provided by the carrier") 	
	private String trackingUrl;

	/** target arrival date<br/><br/><i>Generated property</i> for <code>ConsignmentDataWsDTO.targetArrivalDate</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="targetArrivalDate", value="target arrival date") 	
	private Date targetArrivalDate;

	/** logistics tracking information<br/><br/><i>Generated property</i> for <code>ConsignmentDataWsDTO.trackingEvents</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="trackingEvents", value="logistics tracking information") 	
	private List<ConsignmentEventDataWsDTO> trackingEvents;
	
	public ConsignmentDataWsDTO()
	{
		// default constructor
	}
	
		
	
	public void setStatusDisplay(final String statusDisplay)
	{
		this.statusDisplay = statusDisplay;
	}

		
	
	public String getStatusDisplay() 
	{
		return statusDisplay;
	}
	
		
	
	public void setCarrierDetails(final CarrierWsDTO carrierDetails)
	{
		this.carrierDetails = carrierDetails;
	}

		
	
	public CarrierWsDTO getCarrierDetails() 
	{
		return carrierDetails;
	}
	
		
	
	public void setTrackingID(final String trackingID)
	{
		this.trackingID = trackingID;
	}

		
	
	public String getTrackingID() 
	{
		return trackingID;
	}
	
		
	
	public void setTrackingUrl(final String trackingUrl)
	{
		this.trackingUrl = trackingUrl;
	}

		
	
	public String getTrackingUrl() 
	{
		return trackingUrl;
	}
	
		
	
	public void setTargetArrivalDate(final Date targetArrivalDate)
	{
		this.targetArrivalDate = targetArrivalDate;
	}

		
	
	public Date getTargetArrivalDate() 
	{
		return targetArrivalDate;
	}
	
		
	
	public void setTrackingEvents(final List<ConsignmentEventDataWsDTO> trackingEvents)
	{
		this.trackingEvents = trackingEvents;
	}

		
	
	public List<ConsignmentEventDataWsDTO> getTrackingEvents() 
	{
		return trackingEvents;
	}
	


}
