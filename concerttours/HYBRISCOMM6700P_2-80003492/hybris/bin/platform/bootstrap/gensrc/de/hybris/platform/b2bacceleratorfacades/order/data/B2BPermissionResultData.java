/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:02
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.b2bacceleratorfacades.order.data;

/**
 * @deprecated Since 6.0. Use {@link de.hybris.platform.b2bapprovalprocessfacades.company.data.B2BPermissionResultData} instead.
 */
@Deprecated
public  class B2BPermissionResultData extends de.hybris.platform.b2bapprovalprocessfacades.company.data.B2BPermissionResultData 
{

 
	
	public B2BPermissionResultData()
	{
		// default constructor
	}
	


}
