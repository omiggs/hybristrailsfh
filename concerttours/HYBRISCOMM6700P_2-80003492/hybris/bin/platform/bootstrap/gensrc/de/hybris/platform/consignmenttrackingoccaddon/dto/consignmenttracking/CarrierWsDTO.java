/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 14/09/2018 10:08:01
 * ----------------------------------------------------------------
 *
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.consignmenttrackingoccaddon.dto.consignmenttracking;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Carrier
 */
@ApiModel(value="carrier", description="Carrier")
public  class CarrierWsDTO  implements Serializable 
{

 	/** Default serialVersionUID value. */
 
 	private static final long serialVersionUID = 1L;

	/** carrier code<br/><br/><i>Generated property</i> for <code>CarrierWsDTO.code</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="code", value="carrier code") 	
	private String code;

	/** carrier name<br/><br/><i>Generated property</i> for <code>CarrierWsDTO.name</code> property defined at extension <code>consignmenttrackingoccaddon</code>. */
	@ApiModelProperty(name="name", value="carrier name") 	
	private String name;
	
	public CarrierWsDTO()
	{
		// default constructor
	}
	
		
	
	public void setCode(final String code)
	{
		this.code = code;
	}

		
	
	public String getCode() 
	{
		return code;
	}
	
		
	
	public void setName(final String name)
	{
		this.name = name;
	}

		
	
	public String getName() 
	{
		return name;
	}
	


}
