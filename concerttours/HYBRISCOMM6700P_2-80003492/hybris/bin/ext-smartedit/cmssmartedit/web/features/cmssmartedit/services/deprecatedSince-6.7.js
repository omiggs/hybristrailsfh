/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * 
 * Backwards compatibility for partners and downstream teams
 * The deprecated modules below were moved to cmsSmarteditServicesModule
 * 
 * IMPORANT: THE DEPRECATED MODULES WILL NOT BE AVAILABLE IN FUTURE RELEASES
 * 
 * @deprecated since 6.7
 */
angular.module('AssetsServiceModule', ['cmsSmarteditServicesModule']);
angular.module('cms.smartedit.services', ['cmsSmarteditServicesModule']);
