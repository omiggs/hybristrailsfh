/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/09/2018 10:07:58                         ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.consignmenttrackingservices.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedConsignmenttrackingservicesConstants
{
	public static final String EXTENSIONNAME = "consignmenttrackingservices";
	public static class TC
	{
		public static final String CARRIER = "Carrier".intern();
		public static final String CONSIGNMENTTRACKINGIDVALIDCONSTRAINT = "ConsignmentTrackingIdValidConstraint".intern();
	}
	public static class Attributes
	{
		public static class Consignment
		{
			public static final String CARRIERDETAILS = "carrierDetails".intern();
		}
	}
	public static class Enumerations
	{
		public static class ConsignmentStatus
		{
			public static final String IN_TRANSIT = "IN_TRANSIT".intern();
			public static final String DELIVERING = "DELIVERING".intern();
			public static final String DELIVERY_COMPLETED = "DELIVERY_COMPLETED".intern();
			public static final String DELIVERY_REJECTED = "DELIVERY_REJECTED".intern();
		}
	}
	
	protected GeneratedConsignmenttrackingservicesConstants()
	{
		// private constructor
	}
	
	
}
