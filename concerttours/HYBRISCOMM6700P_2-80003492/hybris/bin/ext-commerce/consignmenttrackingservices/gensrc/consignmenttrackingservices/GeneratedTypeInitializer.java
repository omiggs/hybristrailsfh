

package consignmenttrackingservices;

import java.util.*;
import java.io.Serializable;
import de.hybris.platform.util.*;
import de.hybris.platform.core.*;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.type.*;
import de.hybris.platform.persistence.type.*;
import de.hybris.platform.persistence.enumeration.*;
import de.hybris.platform.persistence.property.PersistenceManager;
import de.hybris.platform.persistence.*;

/**
 * Generated by hybris Platform.
 */
@SuppressWarnings({"cast","unused","boxing","null", "PMD"})
public class GeneratedTypeInitializer extends AbstractTypeInitializer
{
	/**
	 * Generated by hybris Platform.
	 */
	public GeneratedTypeInitializer( ManagerEJB manager, Map params )
	{
		super( manager, params );
	}


	/**
	 * Generated by hybris Platform.
	 */
	@Override
	protected void performRemoveObjects( ManagerEJB manager, Map params ) throws JaloBusinessException
	{
		// no-op by now
	}

	/**
	 * Generated by hybris Platform.
	 */
	@Override
	protected final void performCreateTypes( final ManagerEJB manager, Map params ) throws JaloBusinessException
	{
		// performCreateTypes
	
	
		createItemType(
			"Carrier",
			"GenericItem",
			de.hybris.platform.consignmenttrackingservices.jalo.Carrier.class,
			"de.hybris.platform.persistence.consignmenttrackingservices_Carrier",
			false,
			null,
			false
		);
	
		createItemType(
			"ConsignmentTrackingIdValidConstraint",
			"TypeConstraint",
			de.hybris.platform.consignmenttrackingservices.jalo.ConsignmentTrackingIdValidConstraint.class,
			null,
			false,
			null,
			false
		);
	
	}

	/**
	 * Generated by hybris Platform.
	 */
	@Override
	protected final void performModifyTypes( final ManagerEJB manager, Map params ) throws JaloBusinessException
	{
		// performModifyTypes
	

	
	
				single_createattr_Carrier_code();
			
				single_createattr_Carrier_name();
			
				single_createattr_Consignment_carrierDetails();
			
				single_createattr_ConsignmentTrackingIdValidConstraint_annotation();
			

	}

	
	public void single_createattr_Carrier_code() throws JaloBusinessException
	{
		
						Map sqlColumnDefinitions = null;
					
				createPropertyAttribute(
					"Carrier", 
					"code",  
					null,
					"java.lang.String",
					de.hybris.platform.jalo.type.AttributeDescriptor.READ_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.WRITE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.REMOVE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.SEARCH_FLAG,
					null,
					sqlColumnDefinitions
				);
			
	}
	
	public void single_createattr_Carrier_name() throws JaloBusinessException
	{
		
						Map sqlColumnDefinitions = null;
					
				createPropertyAttribute(
					"Carrier", 
					"name",  
					null,
					"localized:java.lang.String",
					de.hybris.platform.jalo.type.AttributeDescriptor.READ_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.WRITE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.OPTIONAL_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.REMOVE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.SEARCH_FLAG,
					null,
					sqlColumnDefinitions
				);
			
	}
	
	public void single_createattr_Consignment_carrierDetails() throws JaloBusinessException
	{
		
						Map sqlColumnDefinitions = null;
					
				createPropertyAttribute(
					"Consignment", 
					"carrierDetails",  
					null,
					"Carrier",
					de.hybris.platform.jalo.type.AttributeDescriptor.READ_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.WRITE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.OPTIONAL_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.REMOVE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.SEARCH_FLAG,
					null,
					sqlColumnDefinitions
				);
			
	}
	
	public void single_createattr_ConsignmentTrackingIdValidConstraint_annotation() throws JaloBusinessException
	{
		
				redeclareAttribute(
					"ConsignmentTrackingIdValidConstraint", 
					"annotation",
					"java.lang.Class",
					de.hybris.platform.jalo.type.AttributeDescriptor.READ_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.OPTIONAL_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.INITIAL_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.REMOVE_FLAG|de.hybris.platform.jalo.type.AttributeDescriptor.SEARCH_FLAG
				);
			
	}
	


	/**
	 * Generated by hybris Platform.
	 */
	@Override
	protected final void performCreateObjects( final ManagerEJB manager, Map params ) throws JaloBusinessException
	{
		// performCreateObjects
	
	
		createEnumerationValues(
			"ConsignmentStatus",
			true,
			Arrays.asList( new String[] {
			
				"IN_TRANSIT",
				"DELIVERING",
				"DELIVERY_COMPLETED",
				"DELIVERY_REJECTED"
			} )
		);
	
				{
				Map customPropsMap = new HashMap();
				
				setItemTypeProperties(
					"Carrier",
					false,
					true,
					true,
					null,
					customPropsMap
				);
				}
			
			single_setAttributeProperties_Carrier_code();
		
			single_setAttributeProperties_Carrier_name();
		
				{
				Map customPropsMap = new HashMap();
				
				changeMetaType(
					"Consignment",
					null,
					customPropsMap
				);
				}
			
			single_setAttributeProperties_Consignment_carrierDetails();
		
				{
				Map customPropsMap = new HashMap();
				
				setItemTypeProperties(
					"ConsignmentTrackingIdValidConstraint",
					false,
					true,
					true,
					null,
					customPropsMap
				);
				}
			
			single_setAttributeProperties_ConsignmentTrackingIdValidConstraint_annotation();
		
				changeMetaType(
					"ConsignmentStatus",
					null,
					null
				);
			
	}


		
						public void single_setAttributeProperties_Carrier_code() throws JaloBusinessException
						{
							
							
							
							Map customPropsMap = new HashMap();
							
							setAttributeProperties(
								"Carrier", 
								"code",
								true, 
								null,
								null,
								null,
								true,
								true,
								null,
								customPropsMap,
								null
							);
						}
					
						public void single_setAttributeProperties_Carrier_name() throws JaloBusinessException
						{
							
							
							
							Map customPropsMap = new HashMap();
							
							setAttributeProperties(
								"Carrier", 
								"name",
								false, 
								null,
								null,
								null,
								true,
								true,
								null,
								customPropsMap,
								null
							);
						}
					
						public void single_setAttributeProperties_Consignment_carrierDetails() throws JaloBusinessException
						{
							
							
							
							Map customPropsMap = new HashMap();
							
							setAttributeProperties(
								"Consignment", 
								"carrierDetails",
								false, 
								null,
								null,
								null,
								true,
								true,
								null,
								customPropsMap,
								null
							);
						}
					
						public void single_setAttributeProperties_ConsignmentTrackingIdValidConstraint_annotation() throws JaloBusinessException
						{
							
							
							
							Map customPropsMap = new HashMap();
							
							setAttributeProperties(
								"ConsignmentTrackingIdValidConstraint", 
								"annotation",
								false, 
								de.hybris.platform.consignmenttrackingservices.validation.annotations.ConsignmentTrackingIdValid.class,
								"de.hybris.platform.consignmenttrackingservices.validation.annotations.ConsignmentTrackingIdValid.class",
								null,
								true,
								true,
								null,
								customPropsMap,
								null
							);
						}
					
}

	