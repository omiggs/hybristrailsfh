/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/09/2018 10:07:58                         ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.assistedservicestorefront.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAssistedservicestorefrontConstants
{
	public static final String EXTENSIONNAME = "assistedservicestorefront";
	public static class TC
	{
		public static final String ASMCMSUSERGROUPRESTRICTION = "ASMCMSUserGroupRestriction".intern();
		public static final String ASMDEVICESUSEDCOMPONENT = "AsmDevicesUsedComponent".intern();
		public static final String ASMFAVORITECOLORSCOMPONENT = "AsmFavoriteColorsComponent".intern();
		public static final String ASSISTEDSERVICECOMPONENT = "AssistedServiceComponent".intern();
		public static final String ASSISTEDSERVICESESSIONRESTRICTION = "AssistedServiceSessionRestriction".intern();
	}
	
	protected GeneratedAssistedservicestorefrontConstants()
	{
		// private constructor
	}
	
	
}
