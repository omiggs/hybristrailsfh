package concerttours.facades;

import concerttours.data.PedidoData;

import java.util.List;

public interface PedidoFacade {

    void register(String nome, String data, String valor, String quantidade);
    void clear();
    void remove(String key);
    List<PedidoData> getPedidos();

}
