package concerttours.populators;

import concerttours.model.PedidoModel;

public interface PedidoReversePopulator {
    PedidoModel populate(String nome, String data, String valor, String quantidade);
}
