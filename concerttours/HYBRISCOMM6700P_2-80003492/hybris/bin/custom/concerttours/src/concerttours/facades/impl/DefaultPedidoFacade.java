package concerttours.facades.impl;

import concerttours.data.PedidoData;
import concerttours.facades.PedidoFacade;
import concerttours.model.PedidoModel;
import concerttours.populators.PedidoPopulator;
import concerttours.populators.PedidoReversePopulator;
import concerttours.service.PedidoService;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DefaultPedidoFacade implements PedidoFacade {

    @Resource(name = "pedidoReversePopulator")
    private PedidoReversePopulator pedidoReversePopulator;
    @Resource(name = "pedidoPopulator")
    private PedidoPopulator pedidoPopulator;
    private PedidoModel pedido;

    @Resource(name = "pedidoService")
    private PedidoService pedidoService;

    private List<PedidoModel> pedidoModelList;
    private List<PedidoData> pedidoDataList;

    @Override
    public void register(final String nome, final String data, final String valor, final String quantidade) {

        if (!nome.isEmpty() && !data.isEmpty() && !valor.isEmpty() && !quantidade.isEmpty()){
            pedido = pedidoReversePopulator.populate(nome, data, valor, quantidade);
            pedidoService.register(pedido);
        }
    }

    @Override
    public void clear() {
        pedidoService.clear();
    }

    @Override
    public void remove(String key) {
        pedidoService.remove(key);
    }

    @Override
    public List<PedidoData> getPedidos() {
        pedidoModelList = new ArrayList<>();

        pedidoModelList = pedidoService.getPedidos();

        PedidoData pedidoData;
        pedidoDataList = new ArrayList<>();

        try{

            for (PedidoModel p : pedidoModelList){

                //Data
                Date date = p.getDatapedido();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String data = sdf.format(date);


                //Popular
                pedidoData = pedidoPopulator.populate(p.getNumero(), p.getNomecliente(), data, String.valueOf(p.getValorpedido()), String.valueOf(p.getQuantidadeitens()));

                //Salvar no list
                pedidoDataList.add(pedidoData);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("PedidoModelList returns " + pedidoModelList.size() + " items");
        }

        return pedidoDataList;
    }
}
