package concerttours.populators;

import concerttours.data.PedidoData;

public interface PedidoPopulator {
    PedidoData populate(final String numero, final String nome, final String data, final String valor, final String quantidade);
}
