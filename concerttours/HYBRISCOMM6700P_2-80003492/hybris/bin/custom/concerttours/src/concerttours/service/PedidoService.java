package concerttours.service;

import concerttours.model.PedidoModel;

import java.util.List;

public interface PedidoService {
    void register(PedidoModel pedido);
    void clear();
    void remove(String key);
    List<PedidoModel> getPedidos();
}
