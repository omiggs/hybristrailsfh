package concerttours.daos.impl;

import concerttours.daos.PedidoDAO;
import concerttours.model.PedidoModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DefaultPedidoDAO implements PedidoDAO {
    @Autowired
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<PedidoModel> getAll() {
        final String queryString = "SELECT {PK} FROM {Pedido}";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        final SearchResult<PedidoModel> searchResult = flexibleSearchService.search(query);
        return searchResult.getResult();
    }

    @Override
    public void clear() {
        final String queryString = "DELETE FROM {Pedido}";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        final SearchResult<PedidoModel> searchResult = flexibleSearchService.search(query);
        System.out.print(searchResult);
    }
}