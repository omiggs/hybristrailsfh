package concerttours.daos;

import concerttours.model.PedidoModel;

import java.util.List;

public interface PedidoDAO {
    List<PedidoModel> getAll();
    void clear();
}
