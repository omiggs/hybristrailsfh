package concerttours.strategies;

public interface PedidoStrategy {
    String generateRandom8DigitsUniqueCode();
}
