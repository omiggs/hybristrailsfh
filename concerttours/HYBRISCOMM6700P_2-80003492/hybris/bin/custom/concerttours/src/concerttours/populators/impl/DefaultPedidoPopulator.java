package concerttours.populators.impl;

import concerttours.data.PedidoData;
import concerttours.populators.PedidoPopulator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DefaultPedidoPopulator implements PedidoPopulator {
    @Override
    public PedidoData populate(String numero, String nome, String data, String valor, String quantidade) {
        PedidoData pedidoData = new PedidoData();

        //Codigo
        pedidoData.setNumero(numero);

        //Nome
        pedidoData.setNome(nome);

        //Data
        pedidoData.setData(data);

        //Valor
        pedidoData.setValor(valor);

        //Quantidade
        pedidoData.setQuantidade(quantidade);

        return pedidoData;
    }
}
