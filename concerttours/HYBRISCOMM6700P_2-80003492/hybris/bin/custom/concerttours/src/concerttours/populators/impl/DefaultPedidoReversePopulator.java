package concerttours.populators.impl;

import concerttours.model.PedidoModel;
import concerttours.populators.PedidoReversePopulator;

import java.util.Calendar;
import java.util.Date;

public class DefaultPedidoReversePopulator implements PedidoReversePopulator {
    private PedidoModel pedido;
    @Override
    public PedidoModel populate(String nome, String data, String valor, String quantidade) {

        pedido = new PedidoModel();

        pedido.setNomecliente(nome);

        String[] dates = data.split("/");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dates[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(dates[1]) - 1);
        calendar.set(Calendar.YEAR, Integer.valueOf(dates[2]));
        Date date = calendar.getTime();
        pedido.setDatapedido(date);

        pedido.setValorpedido(Double.valueOf(valor));

        pedido.setQuantidadeitens(Integer.valueOf(quantidade));


        return pedido;

    }
}
