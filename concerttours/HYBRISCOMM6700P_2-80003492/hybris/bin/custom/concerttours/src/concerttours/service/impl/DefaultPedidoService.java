package concerttours.service.impl;

import concerttours.daos.PedidoDAO;
import concerttours.model.PedidoModel;
import concerttours.service.PedidoService;
import concerttours.strategies.PedidoStrategy;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;
import java.util.List;

public class DefaultPedidoService implements PedidoService {
    @Resource
    private ModelService modelService;

    @Resource(name = "pedidoDAO")
    private PedidoDAO pedidoDAO;

    @Resource(name = "pedidoStrategy")
    private PedidoStrategy pedidoStrategy;

    @Override
    public void register(PedidoModel pedido) {
        pedido.setNumero(pedidoStrategy.generateRandom8DigitsUniqueCode());
        modelService.save(pedido);
    }

    @Override
    public void clear() {
        //modelService.removeAll(getPedidos());
        //pedidoDAO.clear();
        for (PedidoModel p : getPedidos()){
            modelService.remove(p);
        }
    }

    @Override
    public void remove(String key) {
        for (PedidoModel p : getPedidos()){
            if (p.getNumero().equals(key)){
                modelService.remove(p);
            }
        }
    }

    @Override
    public List<PedidoModel> getPedidos() {
        return pedidoDAO.getAll();
    }
}
