package concerttours.strategies.impl;

import concerttours.strategies.PedidoStrategy;

import java.util.Random;

public class DefaultPedidoStrategy implements PedidoStrategy {
    @Override
    public String generateRandom8DigitsUniqueCode() {

        int random = new Random().nextInt(99999999);

        return String.valueOf(random);
    }
}
