/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 14/09/2018 10:07:58                         ---
 * ----------------------------------------------------------------
 */
package concerttours.jalo;

import concerttours.constants.ConcerttoursConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem Pedido}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedPedido extends GenericItem
{
	/** Qualifier of the <code>Pedido.numero</code> attribute **/
	public static final String NUMERO = "numero";
	/** Qualifier of the <code>Pedido.nomecliente</code> attribute **/
	public static final String NOMECLIENTE = "nomecliente";
	/** Qualifier of the <code>Pedido.datapedido</code> attribute **/
	public static final String DATAPEDIDO = "datapedido";
	/** Qualifier of the <code>Pedido.valorpedido</code> attribute **/
	public static final String VALORPEDIDO = "valorpedido";
	/** Qualifier of the <code>Pedido.quantidadeitens</code> attribute **/
	public static final String QUANTIDADEITENS = "quantidadeitens";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(NUMERO, AttributeMode.INITIAL);
		tmp.put(NOMECLIENTE, AttributeMode.INITIAL);
		tmp.put(DATAPEDIDO, AttributeMode.INITIAL);
		tmp.put(VALORPEDIDO, AttributeMode.INITIAL);
		tmp.put(QUANTIDADEITENS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.datapedido</code> attribute.
	 * @return the datapedido
	 */
	public Date getDatapedido(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, DATAPEDIDO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.datapedido</code> attribute.
	 * @return the datapedido
	 */
	public Date getDatapedido()
	{
		return getDatapedido( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.datapedido</code> attribute. 
	 * @param value the datapedido
	 */
	public void setDatapedido(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, DATAPEDIDO,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.datapedido</code> attribute. 
	 * @param value the datapedido
	 */
	public void setDatapedido(final Date value)
	{
		setDatapedido( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.nomecliente</code> attribute.
	 * @return the nomecliente
	 */
	public String getNomecliente(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NOMECLIENTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.nomecliente</code> attribute.
	 * @return the nomecliente
	 */
	public String getNomecliente()
	{
		return getNomecliente( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.nomecliente</code> attribute. 
	 * @param value the nomecliente
	 */
	public void setNomecliente(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NOMECLIENTE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.nomecliente</code> attribute. 
	 * @param value the nomecliente
	 */
	public void setNomecliente(final String value)
	{
		setNomecliente( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.numero</code> attribute.
	 * @return the numero
	 */
	public String getNumero(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NUMERO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.numero</code> attribute.
	 * @return the numero
	 */
	public String getNumero()
	{
		return getNumero( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.numero</code> attribute. 
	 * @param value the numero
	 */
	public void setNumero(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NUMERO,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.numero</code> attribute. 
	 * @param value the numero
	 */
	public void setNumero(final String value)
	{
		setNumero( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.quantidadeitens</code> attribute.
	 * @return the quantidadeitens
	 */
	public Integer getQuantidadeitens(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, QUANTIDADEITENS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.quantidadeitens</code> attribute.
	 * @return the quantidadeitens
	 */
	public Integer getQuantidadeitens()
	{
		return getQuantidadeitens( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.quantidadeitens</code> attribute. 
	 * @return the quantidadeitens
	 */
	public int getQuantidadeitensAsPrimitive(final SessionContext ctx)
	{
		Integer value = getQuantidadeitens( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.quantidadeitens</code> attribute. 
	 * @return the quantidadeitens
	 */
	public int getQuantidadeitensAsPrimitive()
	{
		return getQuantidadeitensAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.quantidadeitens</code> attribute. 
	 * @param value the quantidadeitens
	 */
	public void setQuantidadeitens(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, QUANTIDADEITENS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.quantidadeitens</code> attribute. 
	 * @param value the quantidadeitens
	 */
	public void setQuantidadeitens(final Integer value)
	{
		setQuantidadeitens( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.quantidadeitens</code> attribute. 
	 * @param value the quantidadeitens
	 */
	public void setQuantidadeitens(final SessionContext ctx, final int value)
	{
		setQuantidadeitens( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.quantidadeitens</code> attribute. 
	 * @param value the quantidadeitens
	 */
	public void setQuantidadeitens(final int value)
	{
		setQuantidadeitens( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.valorpedido</code> attribute.
	 * @return the valorpedido
	 */
	public Double getValorpedido(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, VALORPEDIDO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.valorpedido</code> attribute.
	 * @return the valorpedido
	 */
	public Double getValorpedido()
	{
		return getValorpedido( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.valorpedido</code> attribute. 
	 * @return the valorpedido
	 */
	public double getValorpedidoAsPrimitive(final SessionContext ctx)
	{
		Double value = getValorpedido( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Pedido.valorpedido</code> attribute. 
	 * @return the valorpedido
	 */
	public double getValorpedidoAsPrimitive()
	{
		return getValorpedidoAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.valorpedido</code> attribute. 
	 * @param value the valorpedido
	 */
	public void setValorpedido(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, VALORPEDIDO,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.valorpedido</code> attribute. 
	 * @param value the valorpedido
	 */
	public void setValorpedido(final Double value)
	{
		setValorpedido( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.valorpedido</code> attribute. 
	 * @param value the valorpedido
	 */
	public void setValorpedido(final SessionContext ctx, final double value)
	{
		setValorpedido( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Pedido.valorpedido</code> attribute. 
	 * @param value the valorpedido
	 */
	public void setValorpedido(final double value)
	{
		setValorpedido( getSession().getSessionContext(), value );
	}
	
}
