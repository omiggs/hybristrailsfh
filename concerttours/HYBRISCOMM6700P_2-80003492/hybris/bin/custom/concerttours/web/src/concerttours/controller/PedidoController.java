package concerttours.controller;

import concerttours.daos.PedidoDAO;
import concerttours.data.PedidoData;
import concerttours.facades.PedidoFacade;
import concerttours.model.PedidoModel;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Calendar;

@Controller
public class PedidoController {

    @Resource(name = "pedidoFacade")
    private PedidoFacade pedidoFacade;

    @RequestMapping(value = "/pedido", method = RequestMethod.GET)
    public ModelAndView getPagina(){

        ModelAndView model = new ModelAndView("Pedido");

        model.addObject("listPedidoData", pedidoFacade.getPedidos());

        return model;
    }

    @RequestMapping(value = "/pedido", method = RequestMethod.POST)
    public ModelAndView doRegister(@ModelAttribute("nome") final String nome,
                                   @ModelAttribute("data") final String data,
                                   @ModelAttribute("valor") final String valor,
                                   @ModelAttribute("quantidade") final String quantidade,
                                   @RequestParam("action") final String action){

        ModelAndView model = new ModelAndView("Pedido");

        System.out.print(action);

        if (action.equals("save")){
            pedidoFacade.register(nome, data, valor, quantidade);
        }else if (action.equals("clear")){
            pedidoFacade.clear();
        }else{
            String[] value = {"0" , "0"};
            try{
                value = action.split("/");
                if (value[0].equals("remove")){
                    pedidoFacade.remove(value[1]);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        model.addObject("listPedidoData", pedidoFacade.getPedidos());

        return model;
    }
}