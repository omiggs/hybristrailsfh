<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <title>Cadastro</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
      crossorigin="anonymous">
  </head>

  <body>
    <header>
      <div class="container-fluid mt-5">
        <nav class="nav nav-pills flex-column flex-sm-row">
          <a class="flex-sm-fill text-sm-center nav-link active" href="#">Cadastro</a>
        </nav>
      </div>
    </header>
    <main>
      <div class="container mt-5">
        <h2>
          Cadastrar
        </h2>
        <form action="/concerttours/pedido" method="post">
          <div class="form-group">
            <label >Nome do cliente</label>
            <input name="nome" type="text" class="form-control" aria-describedby="emailHelp" placeholder="Digite o nome do cliente">
          </div>
          <div class="form-group">
            <label >Data</label>
            <input name="data" type="text" class="form-control" placeholder="Data (formato dd/MM/yyyy)">
          </div>
          <div class="form-group">
            <label >Valor total</label>
            <input name="valor" type="text" class="form-control" placeholder="Valor">
          </div>
          <div class="form-group">
            <label >Quantidade</label>
            <input name="quantidade" type="text" class="form-control" placeholder="Quantidade">
          </div>
          <button type="submit" name="action" value="save" class="btn btn-primary" style="width: 15%">Cadastrar</button>

            <br><br>

          <button type="submit" name="action" value="clear" class="btn btn-danger" style="width: 15%">Limpar lista</button>
        </form>
      </div>

      <div class="container mt-5">
        <h2>
          Produtos adicionados
        </h2>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Codigo Pedido</th>
              <th scope="col">Nome cliente</th>
              <th scope="col">Data</th>
              <th scope="col">Valor total</th>
              <th scope="col">Quantidade</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>


            <c:forEach var="listPedidoData" items="${listPedidoData }">
            	          <tr>
            	              <th scope="row" class="align-middle">${listPedidoData.numero }</th>
            	              <td class="align-middle">${listPedidoData.nome }</td>
            	              <td class="align-middle">${listPedidoData.data }</td>
            	              <td class="align-middle">${listPedidoData.valor }</td>
                              <td class="align-middle">${listPedidoData.quantidade }</td>
                              <td class="align-middle">
                                <form action="/concerttours/pedido" method="post">
                                  <button type="submit" name="action" value="remove/${listPedidoData.numero }" class="btn btn-danger" style="width: 30px; height: 30px; padding: 6px 0px; border-radius: 15px; text-align: center; font-size: 12px; line-height: 1.42857;">X</button>
                                </form>
                              </td>
            	            </tr>
                      </c:forEach>



          </tbody>
        </table>
      </div>
    </main>

    <br>
    <br>
    <br>
    <hr>
    <footer class="text-center">
      <h2>fh.com.br</h2>
      <h4>Academia estagiários FH 2018</h4>
      <h6>Jose Otavio Miguel</h6>
    </footer>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
      crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
      crossorigin="anonymous"></script>
    <script src="" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
      crossorigin="anonymous"></script>
  </body>

</html>
