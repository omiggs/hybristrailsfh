/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.webservices.objectgraphtransformer;

public class UriPropertyConfig
{
	// fallback resource
	private final Class rootResourceClass = null;


	private final String locatorMethod = null;

	public Class getRootResourceClass()
	{
		return this.rootResourceClass;
	}


	public String getLocatorMethod()
	{
		return this.locatorMethod;
	}



}
