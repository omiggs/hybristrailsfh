/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.webservices.util.objectgraphtransformer;

public class GraphException extends RuntimeException
{

	public GraphException()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public GraphException(final String message, final Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GraphException(final String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GraphException(final Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
