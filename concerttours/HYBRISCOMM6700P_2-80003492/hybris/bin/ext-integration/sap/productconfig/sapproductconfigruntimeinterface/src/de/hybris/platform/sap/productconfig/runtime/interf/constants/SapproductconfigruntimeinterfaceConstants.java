/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.interf.constants;


/**
 * Global class for all Sapproductconfigruntimeinterface constants.
 */
@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public final class SapproductconfigruntimeinterfaceConstants extends GeneratedSapproductconfigruntimeinterfaceConstants
{
	/**
	 * Name of the sapproductconfigruntimeinterface extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigruntimeinterface";

	/**
	 * Backoffice field name, that defines, which basesite to use for the rule editor
	 */
	public static final String RULE_BASESITE = "sapproductconfig_basesite_cps";


	private SapproductconfigruntimeinterfaceConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
