/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.b2bfrontend.constants;

/**
 * Global class for all Sapproductconfigb2baddon constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings(
{ "PMD", "squid:CallToDeprecatedMethod" })
public final class Sapproductconfigb2baddonConstants extends GeneratedSapproductconfigb2baddonConstants
{
	/**
	 * unique name of this extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigb2baddon";

	private Sapproductconfigb2baddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
