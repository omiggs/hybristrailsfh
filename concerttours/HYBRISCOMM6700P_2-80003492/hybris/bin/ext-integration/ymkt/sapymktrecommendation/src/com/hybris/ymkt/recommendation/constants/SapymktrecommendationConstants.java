/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.recommendation.constants;

/**
 * Global class for all Sapymktrecommendation constants. You can add global constants for your extension into this
 * class.
 */
public final class SapymktrecommendationConstants extends GeneratedSapymktrecommendationConstants
{
	public static final String EXTENSIONNAME = "sapymktrecommendation";
	public static final String PRODUCT = "P";
	public static final String CATEGORY = "C";

	private SapymktrecommendationConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
