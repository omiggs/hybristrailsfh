/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.recommendationaddon.constants;


/**
 *
 * Global class for all Sapproductrecommendation constants. You can add global constants for your extension into this
 * class.
 */
public final class SapymktrecommendationaddonConstants
{
	/**
	 * Extension Name sapymktrecommendationaddon
	 */
	public static final String EXTENSIONNAME = "sapymktrecommendationaddon";
	/**
	 * RecommendationComponentCount Session Attribute
	 */
	public static final String RECOMMENDATIONCOMPONENTCOUNT = "RecommendationComponentCount";
	/**
	 * RecommendationContext Session Attribute
	 */
	public static final String RECOMMENDATIONCONTEXT = "RecommendationContext";

	private SapymktrecommendationaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
