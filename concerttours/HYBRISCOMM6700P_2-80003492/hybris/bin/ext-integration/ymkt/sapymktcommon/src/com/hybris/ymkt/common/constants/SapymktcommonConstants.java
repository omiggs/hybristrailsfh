/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.common.constants;

/**
 * Global class for all Sapymktcommon constants. You can add global constants for your extension into this class.
 */
public final class SapymktcommonConstants extends GeneratedSapymktcommonConstants
{
	public static final String EXTENSIONNAME = "sapymktcommon";

	/**
	 * Constant for property containing content personalization consent template ID
	 */
	public static final String PERSONALIZATION_CONSENT_ID = "sapymktconsent.personalize";

	private SapymktcommonConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
