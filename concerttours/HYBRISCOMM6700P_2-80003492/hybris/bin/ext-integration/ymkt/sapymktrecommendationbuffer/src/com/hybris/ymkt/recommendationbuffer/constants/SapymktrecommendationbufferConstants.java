/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.recommendationbuffer.constants;

/**
 * Global class for all Sapymktrecommendationbuffer constants. You can add
 * global constants for your extension into this class.
 */

public final class SapymktrecommendationbufferConstants extends GeneratedSapymktrecommendationbufferConstants {
	public static final String EXTENSIONNAME = "sapymktrecommendationbuffer";

	public static final String GENERIC_RECO_TYPE = "G";
	public static final String RESTRICTED_RECO_TYPE = "R";

	private SapymktrecommendationbufferConstants() {
		// empty to avoid instantiating this constant class
	}

}
